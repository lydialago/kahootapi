<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\View\View;
use AppBundle\Entity\Question;
use AppBundle\Entity\Answer;

class AnswerController extends FOSRestController
{
    /**
    * @Rest\Get("/answer")
    */
    public function getAction()
    {
      $restresult = $this->getDoctrine()->getRepository('AppBundle:Answer')->findAll();
        if ($restresult === null) {
          return new View("there are no answers exist", Response::HTTP_NOT_FOUND);
     }
        return $restresult;
    }

    /**
     * @Rest\Get("/answer/{id}")
     */
    public function idAction($id)
    {
        $singleresult = $this->getDoctrine()->getRepository('AppBundle:Answer')->find($id);
        if ($singleresult === null) {
            return new View("answer not found", Response::HTTP_NOT_FOUND);
        }
        return $singleresult;
    }

    /**
     * @Rest\Get("/answer/{idQuestion}")
     */
    public function idQuestionAction($idQuestion)
    {
        $restresult = $this->getDoctrine()->getRepository('AppBundle:Answer')->findBy(
			['idQuestion' => $idQuestion ],
		);
        if ($restresult === null) {
            return new View("answer not found", Response::HTTP_NOT_FOUND);
        }
        return $restresult;
    }

    /**
    * @Rest\Post("/answer/")
    */
    public function postAction(Request $request)
    {
        $data = new Answer;
        $text = $request->get('text');
        $idQuestion = $request->get('id_question');
        $isCorrect = $request->get('is_correct');

        if(empty($text) || empty($idQuestion) || $isCorrect == null)
        {
            return new View("NULL VALUES ARE NOT ALLOWED", Response::HTTP_NOT_ACCEPTABLE);
        }
        $data->setText($text);
        $data->setIdQuestion($idQuestion);
        $data->setIsCorrect($isCorrect);

        $em = $this->getDoctrine()->getManager();
        $em->persist($data);
        $em->flush();
        return new View("Answer Added Successfully", Response::HTTP_OK);
    }

    /**
     * @Rest\Put("/answer/{id}")
     */
    public function updateAction($id,Request $request)
    { 
        $data = new Answer;
        $text = $request->get('text');
        $idQuestion = $request->get('id_question');
        $isCorrect = $request->get('is_correct');        
        
        $sn = $this->getDoctrine()->getManager();
        $answer = $this->getDoctrine()->getRepository('AppBundle:Answer')->find($id);
        if (empty($answer)) {
            return new View("answer not found", Response::HTTP_NOT_FOUND);
        } 
        elseif(!empty($text)){
            $answer->setText($text);
            $sn->flush();
            return new View("Answer Updated Successfully", Response::HTTP_OK);
        }
        elseif(!empty($text) && empty($idQuestion) && empty($isCorrect)){
            $user->setText($text);
            $sn->flush();
            return new View("Answer Text Updated Successfully", Response::HTTP_OK); 
        }
        elseif(empty($text) && !empty($idQuestion) && empty($isCorrect)){
            $user->setIdQuestion($idQuestion);
            $sn->flush();
            return new View("Answer Id Question Updated Successfully", Response::HTTP_OK); 
        }
        elseif(empty($text) && empty($idQuestion) && !empty($isCorrect)){
            $user->setIsCorrect($isCorrect);
            $sn->flush();
            return new View("Answer Is Correct Updated Successfully", Response::HTTP_OK); 
        }
        else
            return new View("Text cannot be empty", Response::HTTP_NOT_ACCEPTABLE); 
    }

    /**
     * @Rest\Delete("/answer/{id}")
     */
    public function deleteAction($id)
    {
        $data = new Answer;
        $sn = $this->getDoctrine()->getManager();
        $answer = $this->getDoctrine()->getRepository('AppBundle:Answer')->find($id);
        if (empty($answer)) {
            return new View("answer not found", Response::HTTP_NOT_FOUND);
        }
        else {
            $sn->remove($answer);
            $sn->flush();
        }
        return new View("answer deleted successfully", Response::HTTP_OK);
    }

}
