<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\View\View;
use AppBundle\Entity\Question;

class QuestionController extends FOSRestController
{
    /**
    * @Rest\Get("/question")
    */
    public function getAction()
    {
      $restresult = $this->getDoctrine()->getRepository('AppBundle:Question')->findAll();
        if ($restresult === null) {
          return new View("there are no questions exist", Response::HTTP_NOT_FOUND);
     }
        return $restresult;
    }

    /**
     * @Rest\Get("/question/{id}")
     */
    public function idAction($id)
    {
        $singleresult = $this->getDoctrine()->getRepository('AppBundle:Question')->find($id);
        if ($singleresult === null) {
            return new View("question not found", Response::HTTP_NOT_FOUND);
        }
        return $singleresult;
    }

    /**
    * @Rest\Post("/question/")
    */
    public function postAction(Request $request)
    {
        $data = new Question;
        $text = $request->get('text');
        if(empty($text))
        {
            return new View("NULL VALUES ARE NOT ALLOWED", Response::HTTP_NOT_ACCEPTABLE);
        }
        $data->setText($text);
        $em = $this->getDoctrine()->getManager();
        $em->persist($data);
        $em->flush();
        return new View("Question Added Successfully", Response::HTTP_OK);
    }

    /**
     * @Rest\Put("/question/{id}")
     */
    public function updateAction($id,Request $request)
    {
        $data = new Question;
        $text = $request->get('text');
        $sn = $this->getDoctrine()->getManager();
        $question = $this->getDoctrine()->getRepository('AppBundle:Question')->find($id);
        if (empty($question)) {
            return new View("question not found", Response::HTTP_NOT_FOUND);
        } 
        elseif(!empty($text)){
            $question->setText($text);
            $sn->flush();
            return new View("Question Updated Successfully", Response::HTTP_OK);
        }
        /*
        elseif(!empty($name) && empty($role)){
            $user->setName($name);
            $sn->flush();
            return new View("User Name Updated Successfully", Response::HTTP_OK); 
        }
        */
        else    
            return new View("Text cannot be empty", Response::HTTP_NOT_ACCEPTABLE); 
    }

    /**
     * @Rest\Delete("/question/{id}")
     */
    public function deleteAction($id)
    {
        $data = new Question;
        $sn = $this->getDoctrine()->getManager();
        $question = $this->getDoctrine()->getRepository('AppBundle:Question')->find($id);
        if (empty($question)) {
            return new View("question not found", Response::HTTP_NOT_FOUND);
        }
        else {
            $sn->remove($question);
            $sn->flush();
        }
        return new View("question deleted successfully", Response::HTTP_OK);
    }

}
