USE kahoot_api;
CREATE TABLE `question` (
  `id` int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `text` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB;

CREATE TABLE `answer` (
  `id` int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `text` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `id_question` int(11),
  `is_correct` boolean
) ENGINE=InnoDB;


